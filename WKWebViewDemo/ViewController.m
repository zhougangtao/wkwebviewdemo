//
//  ViewController.m
//  WKWebViewDemo
//
//  Created by gtzhou on 2017/9/10.
//  Copyright © 2017年 xyz. All rights reserved.
//

#import <WebKit/WebKit.h>
#import "ViewController.h"
#import "JSBridge.h"

@interface ViewController ()<WKUIDelegate,WKNavigationDelegate,WKScriptMessageHandler>

@property(nonatomic,strong) WKWebView *webView;
@property(nonatomic,strong) NSString *urlStr;
@property (nonatomic, strong) UIProgressView *progressView;
@property (nonatomic, strong) WKNavigation *gobackNavigation;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self.view addSubview:self.webView];
    
    
    // 添加KVO监听
    [self.webView addObserver:self
                   forKeyPath:@"loading"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self
                   forKeyPath:@"title"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    [self.webView addObserver:self
                   forKeyPath:@"estimatedProgress"
                      options:NSKeyValueObservingOptionNew
                      context:nil];
    
    // 添加进入条
    self.progressView = [[UIProgressView alloc] initWithProgressViewStyle:UIProgressViewStyleDefault];;
    self.progressView.frame = self.view.bounds;
    [self.view addSubview:self.progressView];
    self.progressView.backgroundColor = [UIColor redColor];
}

#pragma mark - propery Method
- (WKWebView*)webView {
    if (!_webView) {
        WKWebViewConfiguration *configuration = [JSBridge shareInstance].defaultConfiguration;
        _webView = [[WKWebView alloc] initWithFrame:self.view.bounds configuration:configuration];
        [JSBridge bridgeWebView:_webView webVC:self];
        
        NSURL *path = [[NSBundle mainBundle] URLForResource:@"test" withExtension:@"html"];
        [self.webView loadRequest:[NSURLRequest requestWithURL:path]];
        
        [self.view addSubview:self.webView];
        
        // 这个代理对应的协议方法常用来显示弹窗
        _webView.UIDelegate = self;
        // 导航代理，在这个代理相应的协议方法可以监听加载网页的周期和结果
        // 在这个代理相应的协议方法可以监听加载网页的周期和结果
        _webView.navigationDelegate = self;
        
        _webView.backgroundColor = [UIColor clearColor];
        _webView.opaque = NO;
        // 侧滑返回上一页，侧滑返回不会加载新的数据，选择性开启
        _webView.allowsBackForwardNavigationGestures = YES;
        
        //KVO
        [_webView addObserver:self forKeyPath:@"title" options:NSKeyValueObservingOptionNew context:nil];
        [_webView addObserver:self forKeyPath:@"URL" options:NSKeyValueObservingOptionNew context:nil];
        [_webView addObserver:self forKeyPath:@"estimatedProgress" options:NSKeyValueObservingOptionNew context:nil];
    }
    return _webView;
}

- (void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    // 移除，避免JS_ScriptMessageReceiver被引用
}

#pragma mark - Private Method

//WKUIDelegate: 此代理方法在使用中最好实现，否则遇到网页alert的时候，如果此代理方法没有实现，则不会出现弹框提示
#pragma mark - WKUIDelegate
- (void)webViewDidClose:(WKWebView *)webView {
    NSLog(@"%s", __FUNCTION__);
}

//拦截到window.open()事件,只需要我们在在方法内进行处理
//http://stackoverflow.com/questions/25713069/why-is-wkwebview-not-opening-links-with-target-blank
- (nullable WKWebView *)webView:(WKWebView *)webView createWebViewWithConfiguration:(WKWebViewConfiguration *)configuration forNavigationAction:(WKNavigationAction *)navigationAction windowFeatures:(WKWindowFeatures *)windowFeatures {
    if (!navigationAction.targetFrame.isMainFrame) {
        [webView loadRequest:navigationAction.request];
    }
    return nil;
}

// 在JS端调用alert函数时，会触发此代理方法。
// JS端调用alert时所传的数据可以通过message拿到
// 在原生得到结果后，需要回调JS，是通过completionHandler回调
- (void)webView:(WKWebView *)webView runJavaScriptAlertPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(void))completionHandler {
    NSLog(@"%s", __FUNCTION__);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"alert" message:@"JS调用alert"                 preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler();
    }]];
    
    [self presentViewController:alert animated:YES completion:NULL];
    NSLog(@"%@", message);
}

// JS端调用confirm函数时，会触发此方法
// 通过message可以拿到JS端所传的数据
// 在iOS端显示原生alert得到YES/NO后
// 通过completionHandler回调给JS端
- (void)webView:(WKWebView *)webView runJavaScriptConfirmPanelWithMessage:(NSString *)message initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(BOOL result))completionHandler {
    NSLog(@"%s", __FUNCTION__);
    
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"confirm" message:@"JS调用confirm" preferredStyle:UIAlertControllerStyleAlert];
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(YES);
    }]];
    [alert addAction:[UIAlertAction actionWithTitle:@"取消" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        completionHandler(NO);
    }]];
    [self presentViewController:alert animated:YES completion:NULL];
    
    NSLog(@"%@", message);
}

// JS端调用prompt函数时，会触发此方法
// 要求输入一段文本
// 在原生输入得到文本内容后，通过completionHandler回调给JS
- (void)webView:(WKWebView *)webView runJavaScriptTextInputPanelWithPrompt:(NSString *)prompt defaultText:(nullable NSString *)defaultText initiatedByFrame:(WKFrameInfo *)frame completionHandler:(void (^)(NSString * __nullable result))completionHandler {
    NSLog(@"%s", __FUNCTION__);
    
    NSLog(@"%@", prompt);
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"textinput" message:@"JS调用输入框" preferredStyle:UIAlertControllerStyleAlert];
    [alert addTextFieldWithConfigurationHandler:^(UITextField * _Nonnull textField) {
        textField.textColor = [UIColor redColor];
    }];
    
    [alert addAction:[UIAlertAction actionWithTitle:@"确定" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        completionHandler([[alert.textFields lastObject] text]);
    }]];
    
    [self presentViewController:alert animated:YES completion:NULL];
}


#pragma mark - WKNavigationDelegate
// 与UIWebView的
// - (BOOL)webView:(UIWebView *)webView
// shouldStartLoadWithRequest:(NSURLRequest *)request
// navigationType:(UIWebViewNavigationType)navigationType;


// 请求开始前，会先调用此代理方法
// 类型，在请求先判断能不能跳转（请求）
// 发送请求前决定是否跳转，并在此拦截拨打电话的URL
//HTML不能通过<a href="tel:123456789">拨号</a>调iOS拨打电话的功能，
//需要我们在WKNavigationDelegate协议方法中截取URL中的号码再拨打电话。
// 发送请求前决定是否跳转，并在此拦截拨打电话的URL,跳转appStore
- (void)webView:(WKWebView *)webView decidePolicyForNavigationAction:(WKNavigationAction *)navigationAction decisionHandler:(void (^)(WKNavigationActionPolicy))decisionHandler {
    NSURL *URL = navigationAction.request.URL;
    NSString *scheme = [URL scheme];
    NSString *hostname = navigationAction.request.URL.host.lowercaseString;
    UIApplication *app = [UIApplication sharedApplication];
    
    if (navigationAction.navigationType == WKNavigationTypeLinkActivated
        && ![hostname containsString:@".baidu.com"]) {
        // 对于跨域，需要手动跳转
        [app openURL:navigationAction.request.URL];
        // 不允许web内跳转
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    else if ([scheme isEqualToString:@"tel"] || [scheme isEqualToString:@"sms"]) {
        NSString *resourceSpecifier = [URL resourceSpecifier];
        NSString *callPhone = [NSString stringWithFormat:@"telprompt://%@", resourceSpecifier];
        /// 防止iOS 10及其之后，拨打电话系统弹出框延迟出现
        dispatch_async(dispatch_get_global_queue(0, 0), ^{
            [app openURL:[NSURL URLWithString:callPhone]];
        });
        decisionHandler(WKNavigationActionPolicyCancel);
        return;
    }
    else if ([URL.absoluteString containsString:@"ituns.apple.com"]) {
        if ([app canOpenURL:URL]) {
            [app openURL:URL];
            decisionHandler(WKNavigationActionPolicyCancel);
            return;
        }
    }
    else {
        if(navigationAction.targetFrame == nil) {
            [webView loadRequest:navigationAction.request];
        }
    }
    
    //允许操作
    self.progressView.alpha = 1.0;
    if (decisionHandler) {
        decisionHandler(WKNavigationActionPolicyAllow);
    }
    NSLog(@"%s", __FUNCTION__);
}

// 在响应完成时，会回调此方法
// 如果设置为不允许响应，web内容就不会传过来
// 收到响应后决定是否跳转
- (void)webView:(WKWebView *)webView decidePolicyForNavigationResponse:(WKNavigationResponse *)navigationResponse decisionHandler:(void (^)(WKNavigationResponsePolicy))decisionHandler {
    decisionHandler(WKNavigationResponsePolicyAllow);
    NSLog(@"%s", __FUNCTION__);
}

// 开始导航跳转时会回调
- (void)webView:(WKWebView *)webView didStartProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    NSLog(@"%s", __FUNCTION__);
}

// 接收到重定向时会回调
- (void)webView:(WKWebView *)webView didReceiveServerRedirectForProvisionalNavigation:(null_unspecified WKNavigation *)navigation {
    NSLog(@"%s", __FUNCTION__);
}

// 导航失败时会回调
- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error{
    if (error.code == NSURLErrorNotConnectedToInternet) {
        /// 无网络(APP第一次启动并且没有得到网络授权时可能也会报错)
    }
    else if (error.code == NSURLErrorCancelled){
        /// -999 上一页面还没加载完，就加载当下一页面，就会报这个错。
        return;
    }
    NSLog(@"webView加载失败:error %@",error);
}

// 页面内容到达main frame时回调
// 内容开始加载
- (void)webView:(WKWebView *)webView didCommitNavigation:(WKNavigation *)navigation{
    NSLog(@"%s", __FUNCTION__);
    self.progressView.alpha = 1.0;
}

// 导航完成时，会回调（也就是页面载入完成了）
// 根据self.gobackNavigation重载页面
- (void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    NSLog(@"%s", __FUNCTION__);
    
    if (self.progressView.progress < 1.0) {
        [UIView animateWithDuration:0.1 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
            self.progressView.alpha = 0.0f;
        } completion:nil];
    }
    
    // 禁止长按弹窗，UIActionSheet样式弹窗
    [webView evaluateJavaScript:@"document.documentElement.style.webkitTouchCallout='none';" completionHandler:nil];
    // 禁止长按弹窗，UIMenuController样式弹窗（效果不佳）
    [webView evaluateJavaScript:@"document.documentElement.style.webkitUserSelect='none';" completionHandler:nil];
    
    // 重载刷新
    if ([navigation isEqual:self.gobackNavigation] || !navigation) {
        [self.webView reload];
        self.gobackNavigation = nil;
    }
    
    // 加载完成
    if ((webView == self.webView) && (!self.webView.loading)) {
        // 手动调用JS代码
        // 每次页面完成都弹出来，大家可以在测试时再打开
        NSString *js = @"callJsAlert()";
        [self.webView evaluateJavaScript:js completionHandler:^(id _Nullable response, NSError * _Nullable error) {
            NSLog(@"response: %@ error: %@", response, error);
            NSLog(@"call js alert by native");
        }];
        
        [UIView animateWithDuration:0.5 animations:^{
            self.progressView.alpha = 0;
        }];
    }
}

// 导航失败时会回调
- (void)webView:(WKWebView *)webView didFailNavigation:(null_unspecified WKNavigation *)navigation withError:(NSError *)error {
    
}

// 对于HTTPS的都会触发此代理，如果不要求验证，传默认就行
// 如果需要证书验证，与使用AFN进行HTTPS证书验证是一样的
//HTTPS触发，若需要证书验证可处理，若不需要直接回调
- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential *credential))completionHandler {
    NSLog(@"%s", __FUNCTION__);
    
    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
        if ([challenge previousFailureCount] == 0) {
            NSURLCredential *credential = [NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust];
            completionHandler(NSURLSessionAuthChallengeUseCredential, credential);
        }
        else {
            completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
        }
    } else {
        completionHandler(NSURLSessionAuthChallengeCancelAuthenticationChallenge, nil);
    }
}

// 9.0才能使用，web内容处理中断时会触发
//当 WKWebView 总体内存占用过大，页面即将白屏的时候，系统会调用上面的回调函数，
//我们在该函数里执行[webView reload](这个时候 webView.URL 取值尚不为 nil）解决白屏问题。
//在一些高内存消耗的页面可能会频繁刷新当前页面，H5侧也要做相应的适配操作
- (void)webViewWebContentProcessDidTerminate:(WKWebView *)webView {
    NSLog(@"%s", __FUNCTION__);
}


// 发送请求前决定是否跳转，并在此拦截拨打电话的URL
#pragma mark - KVO
- (void)observeValueForKeyPath:(NSString *)keyPath
                      ofObject:(id)object
                        change:(NSDictionary<NSString *,id> *)change
                       context:(void *)context {
    if ([keyPath isEqualToString:@"loading"]) {
        NSLog(@"loading");
    }
    else if ([keyPath isEqualToString:@"title"]) {
        self.title = self.webView.title;
    }
    else if ([keyPath isEqualToString:@"estimatedProgress"]) {
        if (object == self.webView) {
            if (self.webView.estimatedProgress == 1.0) {
                self.progressView.progress = 1.0;
                [UIView animateWithDuration:0.2 delay:0 options:UIViewAnimationOptionCurveEaseInOut animations:^{
                    self.progressView.alpha = 0.0f;
                } completion:nil];
            }
            else {
                self.progressView.progress = self.webView.estimatedProgress;
            }
        }
    }
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    if ([message.name isEqualToString:@"JSBridgeEvent"]) {
        // 打印所传过来的参数，只支持NSNumber, NSString, NSDate, NSArray,
        // NSDictionary, and NSNull类型
        NSLog(@"%@", message.body);
    }
}

//// JS代码
//NSString *jScript = @"var meta = document.createElement('meta'); meta.setAttribute('name', 'viewport'); meta.setAttribute('content', 'width=device-width'); document.getElementsByTagName('head')[0].appendChild(meta);";
//// 生成 UScript
//WKUserScript *wkUScript = [[WKUserScript alloc] initWithSource:jScript injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
//// 生成 UController
//WKUserContentController *wkUController = [[WKUserContentController alloc] init];
//// 添加 JS
//[wkUController addUserScript:wkUScript];
//[wkUController addScriptMessageHandler:self name:@"webShare"];


//执行goBack或reload或goToBackForwardListItem后马上执行loadRequest，即一起执行，在didFailProvisionalNavigation方法中会报错，error.code = -999（ NSURLErrorCancelled）。
//[self.webView goBack];
//[self.webView loadRequest:[[NSURLRequest alloc] initWithURL:URL]];
//原因是上一页面还没加载完，就加载当下一页面，会报-999错误。
//
//- (void)webView:(WKWebView *)webView didFailProvisionalNavigation:(WKNavigation *)navigation withError:(NSError *)error{
//    if (error.code == NSURLErrorCancelled){
//        /// -999
//        return;
//    }
//}
//解决方案是在执行goBack或reload或goToBackForwardListItem后延迟一会儿(0.5秒)再执行loadRequest。
//
//[self.webView goBack];
///// 延迟加载新的url，否则报错-999
//[self excuteDelayTask:0.5 InMainQueue:^{
//    [self.webView loadRequest:[[NSURLRequest alloc] initWithURL:URL]];
//}];


//如果message.body中无参数，JS代码中需要传个null,不然iOS端不会接受到JS交互,window.webkit.messageHandlers.kJS_Login.postMessage(null)


//https没有做认证，搞了一个投机的方式：
//
//- (void)webView:(WKWebView *)webView didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition disposition, NSURLCredential * _Nullable credential))completionHandler{
//    
//    if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
//        
//        NSURLCredential *card = [[NSURLCredential alloc]initWithTrust:challenge.protectionSpace.serverTrust];
//        
//        completionHandler(NSURLSessionAuthChallengeUseCredential,card);
//    }
//}

///// JSBridge是封装的JS交互桥梁，遵守WKScriptMessageHandler协议
//- (void)reloadWebViewWithUrl:(NSString *)url {
//    // 先移除
//    [[JSBridge shareInstance] removeAllUserScripts];
//    // 再注入
//    [JSBridge shareInstance].userScriptNames = @[kJS_Login,kJS_Logout];
//    // 再加载URL
//    self.urlStr = url;
//    [self.webView loadRequest:[[NSURLRequest alloc] initWithURL:self.urlStr.URL]];
//}
//
//
//- (void)reloadWebViewWithUrl:(NSString *)url backToHomePage:(BOOL)backToHomePage {
//    void (^LoadWebViewBlock)() = ^() {
//        /// 每次加载新url前重新注入JS对象
//        [[JSBridge shareInstance] removeAllUserScripts];
//        [JSBridge shareInstance].userScriptNames = @[kJS_Login,kJS_Logout];
//        self.urlStr = url;
//        [self.webView loadRequest:[[NSURLRequest alloc] initWithURL:self.urlStr.URL]];
//    };
//    
//    if (self.webView.backForwardList.backList.count && backToHomePage) {
//        /// 返回首页再跳转,并且保留WKNavigation对象
//        self.gobackNavigation = [self.webView goToBackForwardListItem:self.webView.backForwardList.backList.firstObject];
//        /// 延迟加载新的url，否则报错-999
//        double delayInSeconds = 0.5;
//        dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, (int64_t)(delayInSeconds * NSEC_PER_SEC));
//        dispatch_after(popTime, dispatch_get_main_queue(), ^(void) {
//            LoadWebViewBlock();
//        });
//    }
//    else {
//        LoadWebViewBlock();
//    }
//}

@end
