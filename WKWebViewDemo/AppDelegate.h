//
//  AppDelegate.h
//  WKWebViewDemo
//
//  Created by gtzhou on 2017/9/10.
//  Copyright © 2017年 xyz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

