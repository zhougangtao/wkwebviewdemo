//
//  JSBridge.h
//  WKWebViewDemo
//
//  Created by 周刚涛 on 2017/9/17.
//  Copyright © 2017年 xyz. All rights reserved.
//

#import <Foundation/Foundation.h>

#define kJS_Login @"js_Login"
#define kJS_Logout @"js_Logout"

@interface JSBridge : NSObject

@property (nonatomic, strong) NSString *jsPath;
@property (nonatomic, weak) WKWebView *webView;
@property (nonatomic, weak) id webViewController;
@property (nonatomic, strong) NSString *injectJS;

+ (instancetype)shareInstance;
+ (void)bridgeWebView:(WKWebView *)webView;
+ (void)bridgeWebView:(WKWebView *)webView webVC:(UIViewController *)controller;

- (WKWebViewConfiguration *)defaultConfiguration;
- (void)sendInfoToNative:(id)params;
- (void)getInfoFromNative:(id)params :(void(^)(id response))callBack;

@end
