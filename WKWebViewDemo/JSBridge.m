//
//  JSBridge.m
//  WKWebViewDemo
//
//  Created by 周刚涛 on 2017/9/17.
//  Copyright © 2017年 xyz. All rights reserved.
//
#import <WebKit/WebKit.h>
#import "JSBridge.h"

NSString * const kJSBridgeEvent = @"JSBridgeEvent";
NSString * const kJSBridgeEventNew = @"RbJSBridgeEvent";

@interface JSBridge ()<WKScriptMessageHandler>

@property (nonatomic, weak) WKUserContentController * userContentController;
@property (nonatomic, weak) UIViewController *webVC;

@end

@implementation JSBridge

static JSBridge *manager = nil;
+ (instancetype)shareInstance {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        manager = [[JSBridge alloc] init];
        manager.injectJS = [manager getRBJsString];
    });
    return manager;
}

#pragma mark - public
+ (void)bridgeWebView:(WKWebView *)webView {
    [JSBridge shareInstance].webView = webView;
}

+ (void)bridgeWebView:(WKWebView *)webView webVC:(UIViewController *)controller {
    [JSBridge shareInstance].webView = webView;
    if ([UIDevice currentDevice].systemVersion.floatValue >= 9.0f) {
        //The custom user agent string or nil if no custom user agent string has been set
        NSLog(@"webView UserAgent =%@",webView.customUserAgent);
//        [JSBridge updateWKWebViewUserAgent:webView];
    }
    
    if ([controller isKindOfClass:[UIViewController class]]) {
        [JSBridge shareInstance].webVC = (UIViewController *)controller;
    }
}

- (WKWebViewConfiguration *)defaultConfiguration {
    // 偏好配置,涉及JS交互
    WKWebViewConfiguration* configuration = [[WKWebViewConfiguration alloc] init];
    //配置相关
    configuration.preferences = [[WKPreferences alloc] init];
    //表示是否启用了JavaScript 默认YES
    configuration.preferences.javaScriptEnabled = YES;
    //视频播放
    configuration.allowsAirPlayForMediaPlayback = YES;
    //在线播放
    configuration.allowsInlineMediaPlayback = YES;
    // 默认为0
    configuration.preferences.minimumFontSize = 12;
    
    //The default value is NO
    configuration.allowsInlineMediaPlayback = NO;
    
    //默认不能通过JS自动打开窗口的，必须通过用户交互才能打开
    configuration.preferences.javaScriptCanOpenWindowsAutomatically = YES;
    
    // web内容处理池，由于没有属性可以设置，也没有方法可以调用，不用手动创建
    configuration.processPool = [[WKProcessPool alloc] init];
    //表示网页视图是否被禁用内容呈现，直到它被完全加载到内存中,默认值NO
    configuration.suppressesIncrementalRendering = YES;
    
    //可以交互式创建和修改选择的粒度
    //WKSelectionGranularityDynamic:默认值，选择粒度根据选择而自动变化。
    //WKSelectionGranularityCharacter:选择端点可以放置在任何字符边界。
    //与网页交互, 选择视图
    configuration.selectionGranularity = WKSelectionGranularityDynamic;
    
    //缓存机制
    if ([UIDevice currentDevice].systemVersion.floatValue >= 9.0f) {
        configuration.websiteDataStore = [WKWebsiteDataStore defaultDataStore];
    }
    
    // 内容交互控制器 通过js与webview内容交互配置
    // 我们可以在WKScriptMessageHandler代理中接收到
    WKUserContentController *wkUController = [[WKUserContentController alloc] init];
    configuration.userContentController = wkUController;
    self.userContentController = wkUController;

    // 添加 JS
    //官方文档对于这两个选项的解释:
    //WKUserScriptInjectionTimeAtDocumentStart,注入时机为document的元素生成之后,其他内容load之前.
    //WKUserScriptInjectionTimeAtDocumentEnd,注入时机为document全部load完成,任意子资源load完成之前.
    WKUserScript *usrScript = [[WKUserScript alloc] initWithSource:@"" injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    [wkUController addUserScript:usrScript];
    //add之前先删除
    [self removeUserScriptsWithObjName:kJSBridgeEvent];
    [wkUController addScriptMessageHandler:self name:kJSBridgeEvent];
    
    WKUserScript *usrScriptNew = [[WKUserScript alloc] initWithSource:[JSBridge shareInstance].injectJS injectionTime:WKUserScriptInjectionTimeAtDocumentEnd forMainFrameOnly:YES];
    [wkUController addUserScript:usrScriptNew];
    //add之前先删除
    [self removeUserScriptsWithObjName:kJSBridgeEventNew];
    [wkUController addScriptMessageHandler:self name:kJSBridgeEventNew];

    return configuration;
}

#pragma mark - private
//更改User-Agent
//有时我们需要在User-Agent添加一些额外的信息，这时就要更改默认的User-Agent
//在使用UIWebView的时候，可用如下代码(在使用UIWebView之前执行)全局更改User-Agent
//全局更改User-Agent，也就是说，App内所有的Web请求的User-Agent都被修改。
//替换为WKWebView后更改全局User-Agent可以继续使用上面的一段代码
+ (void)updateUIwebViewUserAgent {
    // 获取默认User-Agent
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectZero];
    NSString *oldAgent = [webView stringByEvaluatingJavaScriptFromString:@"navigator.userAgent"];
    
    // 给User-Agent添加额外的信息
    NSString *newAgent = [NSString stringWithFormat:@"%@;%@", oldAgent, @"extra_user_agent"];
    
    // 设置global User-Agent
    NSDictionary *dictionnary = [[NSDictionary alloc] initWithObjectsAndKeys:newAgent, @"UserAgent", nil];
    [[NSUserDefaults standardUserDefaults] registerDefaults:dictionnary];
}

//用WKWebView获取默认的User-Agent，代码如下:
//在iOS9，WKWebView提供了一个非常便捷的属性去更改User-Agent，就是customUserAgent属性。
//这样使用起来不仅方便，也不会全局更改User-Agent
+ (void)updateWKWebViewUserAgent:(WKWebView *)webView {
    if ([UIDevice currentDevice].systemVersion.floatValue >= 9.0f) {
        //The custom user agent string or nil if no custom user agent string has been set
        NSLog(@"self.webView.customUserAgent =%@",webView.customUserAgent);
        webView.customUserAgent = [NSString stringWithFormat:@"%@;%@", webView.customUserAgent, @"extra_user_agent"];
    }
    else {
        // 获取默认User-Agent
        [webView evaluateJavaScript:@"navigator.userAgent" completionHandler:^(id result, NSError *error) {
            NSString *oldAgent = result;
            // 给User-Agent添加额外的信息
            NSString *newAgent = [NSString stringWithFormat:@"%@;%@", oldAgent, @"extra_user_agent"];
            
            // 设置global User-Agent
            NSDictionary *dictionary = [NSDictionary dictionaryWithObjectsAndKeys:newAgent, @"UserAgent", nil];
            [[NSUserDefaults standardUserDefaults] registerDefaults:dictionary];
        }];
    }
}

#pragma mark - private TEST Method
- (NSString *)getRBJsString {
    NSString *path = [[NSBundle mainBundle] pathForResource:@"RbJSBridge" ofType:@"js"];
    NSString *handlerJS = [NSString stringWithContentsOfFile:path encoding:kCFStringEncodingUTF8 error:nil];
    return handlerJS;
}

- (void)sendInfoToNative:(id)params {
    NSLog(@"sendInfoToNative :%@",params);
}

- (void)getInfoFromNative:(id)params :(void(^)(id response))callBack {
    NSLog(@"params %@",params);
    NSString *str = @"'Hi Jack!'";
    callBack(str);
}

- (void)interactWitMethodName:(NSString *)methodName params:(NSDictionary *)params callback:(void(^)(id response))callBack {
    NSMutableArray *paramArray = [[NSMutableArray alloc] init];
    if (params != nil) {
        [paramArray addObject:params];
    }
    if (callBack != nil) {
        [paramArray addObject:callBack];
    }
    
    for (NSInteger i=0; i<paramArray.count; i++) {
        methodName = [NSString stringWithFormat:@"%@:",methodName];
    }
    SEL selector =NSSelectorFromString(methodName);
    if ([self respondsToSelector:selector]) {
        [self performSelector:selector withObjects:paramArray];
    }
}

- (id)performSelector:(SEL)aSelector withObjects:(NSArray *)objects {
    NSMethodSignature *signature = [self methodSignatureForSelector:aSelector];
    NSInvocation *invocation = [NSInvocation invocationWithMethodSignature:signature];
    [invocation setTarget:self];
    [invocation setSelector:aSelector];
    
    NSUInteger i = 1;
    
    for (id object in objects) {
        id tempObject = object;
        [invocation setArgument:&tempObject atIndex:++i];
    }
    [invocation invoke];
    
    if ([signature methodReturnLength]) {
        id data;
        [invocation getReturnValue:&data];
        return data;
    }
    return nil;
}

#pragma mark - WKScriptMessageHandler
- (void)userContentController:(WKUserContentController *)userContentController
      didReceiveScriptMessage:(WKScriptMessage *)message {
    if ([message.name isEqualToString:kJSBridgeEvent]) {
        if ([message.body isKindOfClass:[NSDictionary class]]) {
            NSLog(@"JS调iOS  name : %@    body : %@",message.name,message.body);
        }
    }
    else if ([message.name isEqualToString:kJSBridgeEventNew]) {
        if ([message.body isKindOfClass:[NSString class]] && [message.body hasPrefix:@"_QUEUE_SET_RESULT&"]) {
            NSString *string = (NSString *)message.body;
            NSString *dataStr = [string substringWithRange:NSMakeRange(18, string.length - 18)];
            NSData *data = [[NSData alloc] initWithBase64EncodedString:dataStr options:0];
            id result = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:nil];
            if ([result isKindOfClass:[NSDictionary class]]) {
                NSDictionary *dict = (NSDictionary *)result;
                NSString *methodName = dict[@"func"];
                NSDictionary *params = dict[@"params"];
                NSString *callBackId = dict[@"__callback_id"];
                BOOL needCallback = [dict[@"needCallback"] boolValue];
                
                //把webView和webVC拼进params, 交互方法可能需要该参数
                NSMutableDictionary *sendParams = nil;
                if ([params isKindOfClass:[NSDictionary class]]) {
                    sendParams = [NSMutableDictionary dictionaryWithDictionary:params];
                    if (_webView != nil) {
                        [sendParams setObject:_webView forKey:@"webView"];
                    }
                    if (_webVC != nil) {
                        [sendParams setObject:_webVC forKey:@"webVC"];
                    }
                }
                
                if (needCallback) {
                    __weak  WKWebView *weakWebView = _webView;
                    [self interactWitMethodName:methodName params:sendParams callback:^(id response) {
                        NSString *js = [NSString stringWithFormat:@"RbJSBridge._handleMessageFromApp('%@','%@');", callBackId, response];
                        dispatch_async(dispatch_get_main_queue(), ^{
                            [weakWebView evaluateJavaScript:js completionHandler:^(id _Nullable data, NSError * _Nullable error) {
                            }];
                        });
                    }];
                } else {
                    [self interactWitMethodName:methodName params:sendParams callback:nil];
                }
            }
        }
    }
}

// 移除JS MessageHandler
//没移除ScriptMessageHandler导致内存泄露
- (void)removeUserScriptsWithObjName:(NSString*)name {
    [self.userContentController removeScriptMessageHandlerForName:name];
}

@end
